import { Component, Output, EventEmitter } from '@angular/core';
import { Task } from '../../Task';

@Component({
  selector: 'app-add-task',
  templateUrl: './add-task.component.html',
  styleUrls: ['./add-task.component.css']
})
export class AddTaskComponent {
  @Output() onAddEmit: EventEmitter<Task> = new EventEmitter();
  text: string;
  day: string;
  reminder: boolean = false;

  onSubmit() {
    if(!this.text) {
      alert('Please add a Task!');
      return;
    }   
    
    const newTask = {
      text: this.text,
      day: this.day,
      reminder: this.reminder
    }

    this.onAddEmit.emit(newTask);

    this.text = '';
    this.day = '';
    this.reminder = false;
  }
}

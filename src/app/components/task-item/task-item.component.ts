import { Component, Input, Output, EventEmitter } from '@angular/core';
import { Task } from '../../Task';
import { faTimes } from '@fortawesome/free-solid-svg-icons';
@Component({
  selector: 'app-task-item',
  templateUrl: './task-item.component.html',
  styleUrls: ['./task-item.component.css']
})
export class TaskItemComponent {
  @Input() task: Task;
  @Output() onDeleteEmit: EventEmitter<Task> = new EventEmitter();
  @Output() onToggleEmit: EventEmitter<Task> = new EventEmitter();
  faTimes = faTimes;
  onDelete(task) {
    this.onDeleteEmit.emit(task);
  }

  onToggleReminder(task) {
    this.onToggleEmit.emit(task);
  }
}
